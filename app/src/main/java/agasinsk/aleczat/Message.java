package agasinsk.aleczat;

import java.util.Date;

/**
 * Created by root on 10.04.17.
 */

public class Message {

    private String message;
    private String userID;
    private long messageDate;
    private boolean isLeft;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(long messageDate) {
        this.messageDate = messageDate;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
    }

    public Message(final String message, final String userID, final boolean isLeft) {
        this.message = message;
        this.userID = userID;
        this.isLeft = isLeft;
        this.messageDate = new Date().getTime();

    }
}
